module.exports = {
  'src/containers': `
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import CSSModules from 'react-css-modules';
import classes from './#COMPONENTNAME#.scss';

@CSSModules(classes)
class #COMPONENTNAME# extends Component {
  render() {
    return (
      <div styleName="#COMPONENTNAME#">

      </div>
    );
  }
}

#COMPONENTNAME#.propTypes = {};
#COMPONENTNAME#.defaultProps = {};

export default #COMPONENTNAME#;
`,
  'src/components': `
import React from 'react';
import PropTypes from 'prop-types';
import CSSModules from 'react-css-modules';
import classes from './#COMPONENTNAME#.scss';

const #COMPONENTNAME# = (props) => {
  return (
    <div styleName="#COMPONENTNAME#">

    </div>
  );
};

#COMPONENTNAME#.propTypes = {};
#COMPONENTNAME#.defaultProps = {};

export default CSSModules(#COMPONENTNAME#, classes);

`,
  style: `
.#COMPONENTNAME# {
	border: 1px solid red;
	min-height: 10px;
}
`
};