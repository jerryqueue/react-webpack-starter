import React from 'react';
import CSSModules from 'react-css-modules';
import classes from './Spinner.scss';

const Spinner = (props) => {
  return (
    <div styleName="Spinner">
      <div styleName="spinner-2"></div>
    </div>
  );
};

Spinner.propTypes = {};
Spinner.defaultProps = {};

export default CSSModules(Spinner, classes);