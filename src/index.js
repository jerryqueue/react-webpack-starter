import React from 'react';
import ReactDOM from 'react-dom';
import 'typeface-roboto';
import 'fonts/material-icons.scss';
import './index.scss';
import App from './App';

ReactDOM.render(<App />, document.getElementById('root'));
