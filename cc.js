var path = require('path');
var fs = require('fs');
var templates = require('./templates');
var type = process.argv[2];
var compName = process.argv[3];

function createDirAndFiles(subdirname, compName) {
  const dirpath = path.join(__dirname, subdirname, compName);
  const jsFileName = `${dirpath}/${compName}.js`;
  const styleFileName = `${dirpath}/${compName}.scss`;
  const jsTemplate = (templates[subdirname] || templates['src/components']).trim();
  const styleTemplate = templates.style.trim();

  fs.mkdirSync(dirpath);
  fs.writeFileSync(jsFileName, jsTemplate.replace(/#COMPONENTNAME#/g, compName));
  console.log(`File ${jsFileName} created.`);
  fs.writeFileSync(styleFileName, styleTemplate.replace(/#COMPONENTNAME#/g, compName));
  console.log(`File ${styleFileName} created`);
}
(function() {
  var subdirname = '';

  switch(type.toLowerCase()) {
    case '-c':
      subdirname = 'src/containers';
      break;
    case '-p':
      subdirname = 'src/components';
      break;
    case '-u':
      subdirname = 'src/components/UI';
      break;
    default: break;
  }
  createDirAndFiles(subdirname, compName);
}());