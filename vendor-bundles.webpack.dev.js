const merge = require('webpack-merge');
const common = require('./vendor-bundles.webpack.common');
const path = require('path');
const webpack = require('webpack');

module.exports = merge(common, {
  entry: {
    vendor: ['react-dom', 'react-router-dom', 'material-ui'],
  },
  plugins: [
    new webpack.DllPlugin({
      path: path.join(__dirname, 'dev-build', '[name]-manifest.json'),
      name: '[name]_[hash]',
    }),
  ]
});