To get started, cd into the project root directory, then `npm install` to install dependencies.

_**For development**_, run
```
npm start
```
_**For production**_, run
```
npm run build
```
Afterwards, all you need is the _dist_ folder for deployment.

To create components with ease, run the following commands in the project root directory:
```
node cc -c Demo
```
This will create the **Demo** directory and related boilerplate files in the _**src/containers**_ directory.

```
node cc -p Demo
```
This will create the **Demo** directory and related boilerplate files in the _**src/components**_ directory.

```
node cc -u Demo
```
This will create the **Demo** directory and related boilerplate files in the _**src/components/UI**_ directory.
